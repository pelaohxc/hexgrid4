﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour {
    private int puntaje;
    public int nroMovimientos;
    public bool asignoMovimientos;
    public Text txtMov;

    public HexGrid grid;

    public static bool GameOver = false;
    public static bool Win = false;

    public int celdasVisibles;
    public int celdasOK = 0;

    // Use this for initialization
    void Start () {
        asignoMovimientos = false;
        GameOver = false;
        Win = false;
        nroMovimientos = 0;
        AsignarMovimientos();
        obtenerCeldasVisibles();
	}

    private void obtenerCeldasVisibles()
    {
        for (int i = 0; i < grid.celdas.Length;i++)
        {
            if (grid.celdas[i].tag.Equals("Visible"))
            {
                celdasVisibles++;
            }
        }
    }

    // Update is called once per frame
    void Update () {

        //Destroy(GameObject.FindGameObjectWithTag("Invisible"));

        if (!GameOver)
        {
            if (asignoMovimientos)
            {
                if (nroMovimientos == 0)
                {
                    GameOver = true;
                }else
                {
                    CheckWinParameters();
                    
                }
            }
        }else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadSceneAsync(0);
            }
        }
        if (nroMovimientos > 0)
        {
            txtMov.text = "Movimientos: " + nroMovimientos;
        }
        else
        {
            txtMov.text = "Game Over";
        }
        if (Win)
        {
            Debug.Log("Congratulations you win!");
            Time.timeScale = 0;
        }

    }

    private void CheckWinParameters()
    {
        if (celdasOK == celdasVisibles)
        {
            Win = true;
        }
    }

    public void AsignarMovimientos()
    {
        if (asignoMovimientos == false)
        {
            nroMovimientos = (int)GameObject.FindGameObjectWithTag("Grid").GetComponent<HexGrid>().cantidadPiezas;
            asignoMovimientos = true;
        }
    }
}
