﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
    public GameObject myPrefab;
    public Transform spawnPoint;
	// Use this for initialization
	void Start () {
        spawnPoint = this.transform;
        CreateGrid();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CreateGrid()
    {
        GameObject newEmptyGameObject = new GameObject("Grid");
        // following line is probably not neccessary
        newEmptyGameObject.transform.position = Vector3.zero;

        // some math to find the most left and bottom offset
        float offsetLeft = (-6 / 2f) * 1 + 1 / 2f;
        float offsetBottom = (-6 / 2f) * 1 + 1 / 2f;
        // set it as first spawn position (z=1 because you had it in your script)
        Vector3 nextPosition = new Vector3(offsetLeft, offsetBottom, 1f);

        for (int y = 0; y < 6; y++)
        {
            for (int x = 0; x < 6; x++)
            {
                GameObject clone = Instantiate(myPrefab, nextPosition, Quaternion.identity) as GameObject;
                clone.transform.parent = newEmptyGameObject.transform;
                // add x distance
                nextPosition.x += 1;
            }
            // reset x position and add y distance
            nextPosition.x = offsetLeft;
            nextPosition.y += 1;
        }
        // move the whole grid to the spawnPoint, if there is one
        if (spawnPoint != null)
            newEmptyGameObject.transform.position = spawnPoint.position;
    }
}
