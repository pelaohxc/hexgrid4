﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pieza : MonoBehaviour {
    public HexCell[] Celdas { get; set; }
    public HexCell CellPrefab { get; set; }

    public int ancho, alto;
    public float offset;

    void Awake()
    {
        offset = 1.01f;
        Celdas = new HexCell[ancho * alto];
        for (int z = 0, i = 0; z < alto; z++)
        {
            for (int x = 0; x < ancho; x++)
            {
                CrearCelda(x, z, i++);
            }
        }
        
    }

    private void CrearCelda(int x, int z, int v)
    {
        Vector3 posicion;
        posicion.x = ((x * offset) + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
        posicion.y = 5f;
        posicion.z = (z * offset) * (HexMetrics.outerRadius * 1.5f);

        HexCell celda = Celdas[v] = Instantiate<HexCell>(CellPrefab);
        celda.transform.SetParent(transform, false);
        celda.transform.localPosition = posicion;
        celda.coordenadas = HexCoordinates.FromOffsetCoordinates(x, z);
        celda.color = Color.red;

        if (x > 0)
        {
            celda.SetNeighbor(HexDirection.W, Celdas[v - 1]);
        }
        if (z > 0)
        {
            if ((z & 1) == 0)
            {
                celda.SetNeighbor(HexDirection.SE, Celdas[v - ancho]);
                if (x > 0)
                {
                    celda.SetNeighbor(HexDirection.SW, Celdas[v - ancho - 1]);
                }
            }
            else
            {
                celda.SetNeighbor(HexDirection.SW, Celdas[v - ancho]);
                if (x < ancho - 1)
                {
                    celda.SetNeighbor(HexDirection.SE, Celdas[v - ancho + 1]);
                }
            }
        }
    }

    public void cargarCeldas(List<HexCell> c)
    {
        Celdas = c.ToArray();
        CellPrefab = Celdas[0];
    }
}
