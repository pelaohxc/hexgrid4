﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piezas : MonoBehaviour {

    public GameObject[] piezas;

    private GameObject piezaObj;

    private int index;
    


    void Awake()
    {
        index = 0;
        piezaObj = piezas[index];
        Instantiate<GameObject>(piezaObj).transform.SetParent(transform, false);
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Menu))
        {
            RotarPieza(GameObject.FindGameObjectWithTag("Pieza").GetComponent<PiezaHex>().cantidadCeldas);
        }
        
    }

    private void RotarPieza(int celdas)
    {
        float rotY = GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation.y;
        if (celdas != 3)
        {
            if (rotY == 0)
            {
                GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation = Quaternion.AngleAxis(60, Vector3.up);
                GetComponentInChildren<PiezaHex>().direccion = 2;
                rotY = GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation.y;


            }
            else if(rotY == 0.5)
            {
                GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation = Quaternion.AngleAxis(120, Vector3.up);
                GetComponentInChildren<PiezaHex>().direccion = 3;
                rotY = GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation.y;
            }
            else if(rotY == 0.8660254)
            {
                GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation = Quaternion.AngleAxis(180, Vector3.up);
                GetComponentInChildren<PiezaHex>().direccion = 4;
                rotY = GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation.y;
            }
            else
            {
                GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation = Quaternion.AngleAxis(0, Vector3.down);
                GetComponentInChildren<PiezaHex>().direccion = 1;
                rotY = GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation.y;
            }
            
        }
        else
        {
            if (rotY == 0)
            {
                GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation = Quaternion.AngleAxis(180, Vector3.up);
                GetComponentInChildren<PiezaHex>().direccion = 2;
            }
            else
            {
                GameObject.FindGameObjectWithTag("Pieza").transform.GetChild(0).transform.localRotation = Quaternion.AngleAxis(0, Vector3.down);
                GetComponentInChildren<PiezaHex>().direccion = 1;
            }
        }
    }

    private void MostrarPieza()
    {
        Destroy(GameObject.FindGameObjectWithTag("Pieza"));
        piezaObj = piezas[index];
        Instantiate<GameObject>(piezaObj).transform.SetParent(transform, false);
        
    }

    public void MostrarPieza(bool isDragging)
    {
        //if (!isDragging)
        //{
        //Destroy(GameObject.FindGameObjectWithTag("Pieza"));
            GameObject.FindGameObjectWithTag("Pieza").transform.SetParent(transform.parent,false);
            piezaObj = piezas[index];
            Instantiate<GameObject>(piezaObj).transform.SetParent(transform, false);
        //}
    }

    public void SiguientePieza()
    {
        switch (index)
        {
            case 0:
                index = 1;
                break;
            case 1:
                index = 2;
                break;
            case 2:
                index = 0;
                break;
            default:
                break;
        }
        MostrarPieza();
    }

    public void AnteriorPieza()
    {
        switch (index)
        {
            case 0:
                index = 2;
                break;
            case 1:
                index = 0;
                break;
            case 2:
                index = 1;
                break;
            default:
                break;
        }
        MostrarPieza();
    }

    
}
