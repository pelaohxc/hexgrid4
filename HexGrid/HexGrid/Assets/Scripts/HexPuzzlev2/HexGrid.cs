﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HexGrid : MonoBehaviour {

    public float factorRandom;
    public Sprite Hexagon;

    public int ancho, alto;
    public HexCell cellPrefab;

    private List<HexCoordinates> coordenadasActivas;
    private List<HexCoordinates> coordenadasInactivas;

    public HexCell[] celdas;
    HexMesh hexMesh;

    public float offset;
    public float cantidadPiezas;
    public int factorPiezas;
    public static bool isCreated;

    public HexCell[] listaP;

    void Awake()
    {
        //isCreated = false;
        if (!isCreated)
        {
            //hexMesh = GetComponentInChildren<HexMesh>();

            celdas = new HexCell[ancho * alto];
            coordenadasActivas = new List<HexCoordinates>();
            coordenadasInactivas = new List<HexCoordinates>();

            for (int z = 0, i = 0; z < alto; z++)
            {
                for (int x = 0; x < ancho; x++)
                {
                    CrearCelda(x, z, i++);
                }
            }
        }else
        {
            hexMesh = GetComponentInChildren<HexMesh>();
        }



    }

    
    void Start()
    {
        if (!isCreated)
        {
            //hexMesh.ToggleActivo(true);
            //hexMesh.Triangular(celdas);
            RandomizarGrid();
            //hexMesh.ToggleActivo(false);
        }
        else
        {
            
            generarPieza();
            //hexMesh.ToggleActivo(false);
        }
    }

    public void generarPieza()
    {
        List<HexCell> l = new List<HexCell>();
        for (int i = 0; i < listaP.Length; i++)
        {
            if (listaP[i].tag.Equals("Visible"))
            {
                l.Add(listaP[i]);
            }
        }
        //hexMesh.Triangular(l.ToArray());
        transform.position = GameObject.FindGameObjectWithTag("TableroPiezas").transform.position - GameObject.FindGameObjectWithTag("TableroPiezas").transform.position;
        Vector3 pos = transform.position;
        pos.y = pos.y - 50;
        transform.position = pos;
        gameObject.tag = "Piezas";
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RandomizarGrid();
        }
    }


    private void CrearCelda(int x, int z, int v)
    {
        Vector3 posicion;
        posicion.x = ((x * offset) + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
        posicion.y = 0f;
        posicion.z = (z * offset) * (HexMetrics.outerRadius * 1.5f);

        HexCell celda = celdas[v] = Instantiate<HexCell>(cellPrefab);
        celda.transform.SetParent(transform, false);
        celda.transform.localPosition = posicion;
        celda.coordenadas = HexCoordinates.FromOffsetCoordinates(x, z);
        celda.color = Color.blue;

        if (x > 0)
        {
            celda.SetNeighbor(HexDirection.W, celdas[v - 1]);
        }
        if (z > 0)
        {
            if ((z & 1) == 0)
            {
                celda.SetNeighbor(HexDirection.SE, celdas[v - ancho]);
                if (x > 0)
                {
                    celda.SetNeighbor(HexDirection.SW, celdas[v - ancho - 1]);
                }
            }
            else
            {
                celda.SetNeighbor(HexDirection.SW, celdas[v - ancho]);
                if (x < ancho - 1)
                {
                    celda.SetNeighbor(HexDirection.SE, celdas[v - ancho + 1]);
                }
            }
        }

        //Text label = Instantiate<Text>(cellLabelPrefab);
        //label.rectTransform.SetParent(gridCanvas.transform, false);
        //label.rectTransform.anchoredPosition = new Vector2(posicion.x, posicion.z);
        //label.text = celda.coordenadas.ToStringOnSeparateLines();
    }




    public void ColorCell(Vector3 position, Color color)
    {
        position = transform.InverseTransformPoint(position);
        HexCoordinates coordinates = HexCoordinates.FromPosition(position);
        int index = coordinates.X + coordinates.Z * ancho + coordinates.Z / 2;
        HexCell celda = celdas[index];
        celda.color = color;
        //hexMesh.Triangular(celdas);
    }

    public void RandomizarGrid()
    {
        List<HexCell> randomList = new List<HexCell>();
        for (int i = 0; i < celdas.Length; i++)
        {
            if (UnityEngine.Random.value * 10 > factorRandom)
            {
                randomList.Add(celdas[i]);
                celdas[i].tag = "Visible";
                coordenadasActivas.Add(celdas[i].coordenadas);
                celdas[i].transform.GetChild(0).gameObject.SetActive(true);

            }
            else
            {
                celdas[i].tag = "Invisible";
                //Destroy(celdas[i]);
                coordenadasInactivas.Add(celdas[i].coordenadas);
                celdas[i].transform.GetChild(0).gameObject.SetActive(false);
            }


        }
        verificarPiezasAisladas();

    }

    private void verificarPiezasAisladas()
    {
        bool hasAdyacent;
        List<HexCell> lista = new List<HexCell>();
        for (int i = 0; i < celdas.Length; i++)
        {
            hasAdyacent = false;
            for (int j = 0; j < celdas[i].celdasAdyacentes.Length; j++)
            {
                if (celdas[i].celdasAdyacentes[j] != null)
                {
                    if (celdas[i].celdasAdyacentes[j].tag.Equals("Visible"))
                    {
                        hasAdyacent = true;
                    }
                    //else
                    //{
                    //    hasAdyacent = false;
                    //}
                } else
                {
                    hasAdyacent = false;
                }
            }
            if (hasAdyacent)
            {
                celdas[i].tag = "Visible";
                //celdas[i].transform.GetChild(0).gameObject.active = true;
                lista.Add(celdas[i]);
            } else
            {
                celdas[i].tag = "Invisible";
                celdas[i].transform.GetChild(0).gameObject.SetActive(false);
                //Destroy(celdas[i]);
            }
            if (celdas[i].tag.Equals("Visible"))
            {
                if (celdas[i].transform.GetChild(0).gameObject.activeSelf == false)
                {
                    celdas[i].tag = "Invisible";
                }
            }

        }
        //hexMesh.Triangular(lista.ToArray());
        cantidadPiezas = crearPiezas(lista);
        isCreated = true;
    }

    private float crearPiezas(List<HexCell> randomList)
    {

        foreach (var coordenada in coordenadasActivas)
        {
            for (int i = 0; i < celdas.Length; i++)
            {

                if (celdas[i].coordenadas.Equals(coordenada))
                {
                    //celdas[i].color = Color.blue;
                    foreach (var celda in randomList)
                    {
                        celda.color = Color.white;
                    }
                }
            }
        }
        //hexMesh.Triangular(randomList.ToArray());
        listaP = celdas;
        
        float piezas = (randomList.Count - factorPiezas);
        return piezas;
    }

    
}
