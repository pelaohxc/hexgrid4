﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PiezaHex : MonoBehaviour
{

    public int ancho, alto;
    public HexCell cellPrefab;
    public int cantidadCeldas;

    public int direccion;
    public bool dropped;
    public bool todasLasPiezasOk;

    public RaycastHit hit;

    HexCell[] celdas;
    HexMesh hexMesh;

    public Color defaultColor = Color.white;
    public Color touchedColor = Color.magenta;
    public float offset;

    void Awake()
    {
        todasLasPiezasOk = false;
        direccion = 1;
        hexMesh = GetComponentInChildren<HexMesh>();

        celdas = new HexCell[ancho * alto];

        for (int z = 0, i = 0; z < alto; z++)
        {
            for (int x = 0; x < ancho; x++)
            {
                CrearCelda(x, z, i++);
            }
        }
    }

    internal void cambiarColor(Color o)
    {
        foreach (var celda in celdas)
        {
            celda.color = o;
        }
    }

    void Start()
    {
        hexMesh.Triangular(celdas);
        ActualizarPiezas();
        dropped = false;
        if (cantidadCeldas == 2)
        {
            Vector3 pos = transform.localPosition;
            pos.x = -7;
            transform.localPosition = pos;
        }
        if (cantidadCeldas == 3)
        {
            Vector3 pos = transform.localPosition;
            pos.x = -17;
            pos.y = -10;
            transform.localPosition = pos;
        }
    }

    private void CrearCelda(int x, int z, int v)
    {
        Vector3 posicion;
        posicion.x = ((x * offset) + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
        posicion.y = 5f;
        posicion.z = (z * offset) * (HexMetrics.outerRadius * 1.5f);

        HexCell celda = celdas[v] = Instantiate<HexCell>(cellPrefab);
        celda.transform.SetParent(transform, false);
        celda.transform.localPosition = posicion;
        celda.coordenadas = HexCoordinates.FromOffsetCoordinates(x, z);
        celda.color = Color.red;

        if (x > 0)
        {
            celda.SetNeighbor(HexDirection.W, celdas[v - 1]);
        }
        if (z > 0)
        {
            if ((z & 1) == 0)
            {
                celda.SetNeighbor(HexDirection.SE, celdas[v - ancho]);
                if (x > 0)
                {
                    celda.SetNeighbor(HexDirection.SW, celdas[v - ancho - 1]);
                }
            }
            else
            {
                celda.SetNeighbor(HexDirection.SW, celdas[v - ancho]);
                if (x < ancho - 1)
                {
                    celda.SetNeighbor(HexDirection.SE, celdas[v - ancho + 1]);
                }
            }
        }
    }

    private void ActualizarPiezas()
    {
        List<HexCell> randomList = new List<HexCell>();
        switch (cantidadCeldas)
        {
            case 1:
                break;
            case 2:
                break;
            case 3:
                
                for (int i = 1; i < celdas.Length; i++)
                {
                    randomList.Add(celdas[i]);
                }
                celdas = randomList.ToArray();
                hexMesh.Triangular(randomList.ToArray());
                break;
            default:
                break;
        }

        
    }

    void FixedUpdate()
    {
        if (GetComponent<DragAndDrop>().draggingItem && !GetComponent<DragAndDrop>().isOnTablero)
        {
            buscarCoordenada();
        }
        
    }


    public void buscarCoordenada()
    {
        Vector3 posiscion = this.gameObject.transform.position;
        posiscion.z = posiscion.z - 1;
        Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        
        Debug.DrawRay(posiscion, transform.TransformDirection(Vector3.down) * 100, Color.green);
        if (Physics.Raycast(inputRay, out hit))
        {
            
            if (hit.transform.tag.Equals("Visible"))
            {
                float centro = 10f * 0.866025404f;
                switch (cantidadCeldas)
                {
                    case 1:
                        
                        GetComponent<DragAndDrop>().rayo.transform.position = hit.transform.gameObject.transform.position;
                        break;
                    case 2:
                        Vector3 pos;
                        switch (direccion)
                        {
                            case 1:
                                pos = new Vector3();
                                pos = hit.transform.gameObject.transform.position;
                                pos.x = pos.x - centro * 2;
                                GetComponent<DragAndDrop>().rayo.transform.position = pos;
                                break;
                            case 2:
                                pos = new Vector3();
                                pos = hit.transform.gameObject.transform.position;
                                //pos.x = pos.x - centro;
                                //pos.y += (pos.y - centro * 2)+2; Attention!!!!!!!! only works with bottom cells

                                //transform.position = pos;

                                GetComponent<DragAndDrop>().rayo.transform.position = pos;
                                break;
                            case 3:
                                pos = new Vector3();
                                break;
                            default:
                                break;
                        }
                        break;
                    case 3:
                        Vector3 p;
                        switch (direccion)
                        {
                            case 1:
                                p = new Vector3();
                                p = hit.transform.gameObject.transform.position;
                                p.x = p.x - centro * 2;
                                GetComponent<DragAndDrop>().rayo.transform.position = p;
                                break;
                            case 2:

                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

    }

    public void Destruir()
    {
        Destroy(gameObject);
    }
}
