﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableroCollider : MonoBehaviour {

    public Piezas tablero;
    private bool dragging = true;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Pieza")
        {
            other.gameObject.GetComponent<DragAndDrop>().isOnTablero = false;
            dragging = other.gameObject.GetComponent<DragAndDrop>().draggingItem;
            other.transform.SetParent(transform, false);
            
            tablero.MostrarPieza(dragging);
            other.tag = "PiezaFuera";
            
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "PiezaFuera")
        {
            other.gameObject.GetComponent<DragAndDrop>().isOnTablero = true;
            Destroy(other.gameObject);
            int movs = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorJuego>().nroMovimientos;
            if (!other.gameObject.GetComponent<DragAndDrop>().isOnTablero)
            {
                GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorJuego>().nroMovimientos = movs + 1;
            }
        }
    }


}
