﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HexMesh : MonoBehaviour {
    Mesh hexMesh;
    List<Vector3> vertices;
    List<int> triangulos;
    MeshCollider meshCollider;

    List<Color> colors;

    private void Awake()
    {
        GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
        meshCollider = gameObject.AddComponent<MeshCollider>();
        hexMesh.name = "Hex Mesh";
        vertices = new List<Vector3>();
        triangulos = new List<int>();
        colors = new List<Color>();


    }

    public void Triangular(HexCell[] cells)
    {
        hexMesh.Clear();
        vertices.Clear();
        triangulos.Clear();
        colors.Clear();
        for (int i = 0; i < cells.Length; i++)
        {
            Triangulate(cells[i]);
        }
        hexMesh.vertices = vertices.ToArray();
        hexMesh.triangles = triangulos.ToArray();
        hexMesh.colors = colors.ToArray();
        hexMesh.RecalculateNormals();

        meshCollider.sharedMesh = hexMesh;
    }

    void Triangulate(HexCell cell)
    {
        Vector3 centro = cell.transform.localPosition;
        for (int i = 0; i < 6; i++)
        {
            AgregarTriangulo(centro, centro + HexMetrics.corners[i], centro + HexMetrics.corners[i+1]);
            AgregarColorATriangulo(cell.color);
        }
    }

    private void AgregarColorATriangulo(Color color)
    {
        colors.Add(color);
        colors.Add(color);
        colors.Add(color);
    }

    void AgregarTriangulo(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        int vertexIndex = vertices.Count;
        vertices.Add(v1);
        vertices.Add(v2);
        vertices.Add(v3);
        triangulos.Add(vertexIndex);
        triangulos.Add(vertexIndex + 1);
        triangulos.Add(vertexIndex + 2);
    }

    public void ToggleActivo(bool t)
    {
        gameObject.active = t;
    }
}
