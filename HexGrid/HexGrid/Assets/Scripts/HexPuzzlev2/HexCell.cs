﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexCell : MonoBehaviour {

    public HexCoordinates coordenadas;
    public Color color;
    private Ray gridRay;

    public bool piezaOK;
    public bool asigned;

    [SerializeField]
    public HexCell[] celdasAdyacentes;

    public HexCell GetNeighbor (HexDirection direction) {
		return celdasAdyacentes[(int)direction];
	}

    public void SetNeighbor(HexDirection direction, HexCell cell)
    {
        celdasAdyacentes[(int)direction] = cell;
        cell.celdasAdyacentes[(int)direction.Opposite()] = this;
    }

    

    void Start()
    {
        asigned = false;
        piezaOK = false;
        if (transform.parent.tag.Equals("Grid") || transform.parent.tag.Equals("Piezas"))
        {
            gameObject.AddComponent<BoxCollider>();
            gameObject.GetComponent<BoxCollider>().size = new Vector3(18f, 18f, 18f);
        }
    }

    void FixedUpdate()
    {

        if (gameObject.tag.Equals("Visible"))
        {
            RaycastHit[] hits;
            hits = Physics.RaycastAll(transform.position, -transform.forward, Mathf.Infinity);

            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];

                if (!hit.transform.tag.Equals("Visible") && !hit.transform.tag.Equals("TableroPiezas") && !hit.transform.tag.Equals("Invisible"))
                {
                    if (hit.transform.tag.Equals("PiezaEncima"))
                    {
                        this.piezaOK = true;
                    }
                }

            }

        }
        if (transform.parent.GetComponent<PiezaHex>())
        {
            if (transform.parent.GetComponent<PiezaHex>().dropped)
            {
                Vector3 p = transform.position;
                p.z += p.z;
                Ray ray = new Ray(p, Vector3.forward);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    Debug.Log(hit.transform.tag);
                    if (hit.transform.tag.Equals("Invisible") || hit.transform.tag.Equals("Fondo"))
                    {
                        
                        transform.parent.GetComponent<PiezaHex>().Destruir();
                    }
                }
            }
        }
    }
}
